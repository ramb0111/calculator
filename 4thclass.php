<?php

?>
<!--og is a meta tag defined by facebook which you have to add in your website to appear it on facebook
like in facebook , it tells the facebook to add which pic ,when we are adding in links of youtube in status-->

<!doctype html>
<html>
   <head lang="en">  
       <meta charset="UTF-8">   <!--utf 8 means 8 bit set of characters , 16 for chinses and all-->
       <title>Basic Calculator</title>
   </head>
   <body>  
     <h1>
	    My First Calculator
	 </h1>
	 <form>
	     <label for="first_number">First Name</label>   <!--for & id for highlighting -->
	     <input id="first_number" type="text" name="firstnumber" placeholder="first number here"/>   <!--name for accesssing-->
		   <br>
		   <br>
		  <label for="second_number">Last Name</label>
		  <input id ="second_number" type="password" name="secondnumber" placeholder="second number here"/>
		  <br>
		  <br>
		  <label>Operator:</label>
		  <select name ="operation">
		    <option>add</option>
			<option>subtract</option>
		    <option>multiply</option>
			<option>divide</option>
			<option>power</option>   <!--try radio button too-->
		  </select>
		  <!--git add & commit to change -->
          <!--if u want to remove last change , git checkout previous ones first 6 digit-->
		  <!-- git checkout master will lead to latest commit even if u checkout previous one -->
	 </form>
   </body>
</html>